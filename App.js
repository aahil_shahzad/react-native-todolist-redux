/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import TodoScreen from './App/Screens/TodoScreen'

const App: () => React$Node = () => {
    return (
        <TodoScreen />
    )
};
export default App;
