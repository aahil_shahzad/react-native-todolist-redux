import { ADD_TODO, DELETE_TODO } from './types';

const addTodo = (data) => ({
    type: ADD_TODO,
    data: data
});

const deleteTodo = (key) => ({
    type: DELETE_TODO,
    key: key
});


export { addTodo, deleteTodo };