import { ADD_TODO, DELETE_TODO } from '../actions/types';

const intialState = {
    todoList: []
}

const TodoReducer = (state = intialState, action)  => {
    switch(action.type) {
        case ADD_TODO:
            return {
                ...state,
                todoList: state.todoList.concat({
                    key: Math.floor(Math.random() * 100),
                    todo: action.data
                })
            };
        case DELETE_TODO: 
            return {
                ...state,
                todoList: state.todoList.filter(( item ) => item.key !== action.key )
            };
        default:
            return state;
    }
}

export default TodoReducer;