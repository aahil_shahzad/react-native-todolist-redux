import { createStore, combineReducers } from 'redux';
import TodoReducer from './reducers/TodoReducer';

const rootReducer = combineReducers({
    TodoReducer: TodoReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;