import React,{ Component } from 'react';
import { Container, Right, Header, Content, Form, Item, Input, Label, Button, Icon, Text, View, List, ListItem, Left } from 'native-base';
import { connect } from 'react-redux';
import { addTodo, deleteTodo } from '../redux/actions/todo';
import { TouchableOpacity } from 'react-native';

class TodoScreen extends Component{
    constructor (props) {
        super(props);
        this.state ={
            name: '',
        }
    }
    addTodoList =  () => {
        this.props.addTodo(this.state.name);
    }
    renderItems = (items) => {
        return(
            <ListItem>
                <Left>
                    <Text>{ items.todo }</Text>
                </Left>
                <Right>
                    <TouchableOpacity onPress={ () => this.props.deleteTodo(items.key)}>
                        <Text note>delete</Text>
                    </TouchableOpacity>
                </Right>
            </ListItem>
        );
    }
    render(){
        return (
            <Container>
                <Header />
                <Content>
                    <Form>
                        <Item last>
                            <Label>Name</Label>
                            <Input 
                                onChangeText={(name) => this.setState({name})}
                            />
                        </Item>
                        <View style={{marginTop:40}}>
                            <Button full onPress={() => this.addTodoList() }>
                                <Text>Add</Text>
                            </Button>
                        </View>
                    </Form>
                    <List 
                        enableEmptySections={ true }
                        dataArray={ this.props.todoList }
                        renderRow={ this.renderItems }
                    />
                </Content>
            </Container>
        );
    }
}


const mapStateToProps = ( state ) => {
    return {
        todoList: state.TodoReducer.todoList
    } 
}


const mapDispatchToProps = ( dispatch ) => {
    return {
        addTodo: (data) =>  dispatch(addTodo(data)),
        deleteTodo: (key) =>  dispatch(deleteTodo(key))
    }

}

export default connect( mapStateToProps, mapDispatchToProps )( TodoScreen );